# Meltano project for tap-postgres

> `tap-postgres` doesn't support the `--catalog` options so `meltano elt` doesn't correctly send the catalog. Use `meltano invoke` instead.

To run

```
$ meltano install
$ meltano select tap-postgres --list
$ meltano select tap-postgres 'table'
$ meltano invoke tap-postgres --properties .meltano/run/tap-postgres/tap.properties.json
```
